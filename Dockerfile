# Use the specified version of PHP on Alpine
FROM php:8.2-fpm-alpine

# Install nginx and supervisord
RUN apk update && \
    apk add --no-cache nginx supervisor

# Setup document root
RUN mkdir -p /var/www/html

# Copy over the Nginx and Supervisord configurations
COPY default.conf /etc/nginx/http.d/default.conf
COPY supervisord.conf /etc/supervisord.conf

# Copy the PHP script into the container
COPY index.php /var/www/html/index.php

# Adjust permissions
RUN adduser -D -g 'www' www && \
    chown -R www:www /var/lib/nginx && \
    chown -R www:www /var/www/html && \
    mkdir -p /run/nginx

# Expose port 80 to the outside
EXPOSE 80

# Start Supervisord
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
