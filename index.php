<?php
function sum($a, $b) {
    return $a + $b;
}

function multiply($a, $b) {
    return $a * $b;
}

echo "Hello, World! <br>";
echo "The sum of 4 and 5 is: " . sum(4, 5) . "<br>";
echo "The product of 6 and 7 is: " . multiply(6, 7);
?>
