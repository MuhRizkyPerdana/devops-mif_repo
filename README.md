# devops-mif_repo

# INFRA PROCESS DIAGRAM

![alt text](image-4.png)
ci/cd diagram

# Question Number 1
1. Create docker image with packages
- nginx
- php-fpm
- php version 8.2
- image using linux alpine
- create a simple php hello world (better if you can add more function)
- Push images you have created to your docker-hub private registry with name
devops-mif_images:testing
- push your souce code to your personal github repo name devops-mif_repo


Components
PHP 8.2-FPM: PHP FastCGI Process Manager for handling PHP requests.
Nginx: High-performance web server for serving web content.
Supervisord: Process Control System for controlling multi-process applications.

Dockerfile :
https://gitlab.com/MuhRizkyPerdana/devops-mif_repo/-/blob/main/Dockerfile?ref_type=heads

Nginx Configuration :
https://gitlab.com/MuhRizkyPerdana/devops-mif_repo/-/blob/main/default.conf?ref_type=heads

Supervisord Configuration :
https://gitlab.com/MuhRizkyPerdana/devops-mif_repo/-/blob/main/supervisord.conf?ref_type=heads

 
PHP Script :

https://gitlab.com/MuhRizkyPerdana/devops-mif_repo/-/blob/main/index.php?ref_type=heads


Building the Image
To build the Docker image, run the following command:

shell : 
docker build -t rizkyperdana28/devops-mif_images:testing .

Running the Container
shell :
docker run -d -p 80:80 rizkyperdana28/devops-mif_images:testing

Pushing to Docker Hub 
shell : 
docker login
shell :
docker push rizkyperdana28/devops-mif_images:testing



# Question Number 2

2. Create docker-compose file with using image you previously created
- Port using 80
- Docker-composer with version 3


To run the Docker container using Docker Compose, use the provided `docker-compose.yml` file, which is set up with the necessary configuration to start the service.


To start the service, run:

docker-compose up -d

![alt text](image-2.png)


# Question Number 3

3. Create gitlab-ci.yml with image you have created before

Gitlab ci

https://gitlab.com/MuhRizkyPerdana/devops-mif_repo/-/blob/main/.gitlab-ci.yml

# Question Number 4

4. Create deployment file for kubernetes with image from docker-hub you previously created

Setup the Agent first, i try use minikube

- kubernetes cluster
- connect to cluster
- register agent

code : 

helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install agentminikube gitlab/gitlab-agent \
    --namespace gitlab-agent-agentminikube \
    --create-namespace \
    --set image.tag=v16.9.2 \
    --set config.token=glagent-hpdcRnU4RJDS8VBjZWGK82B_q7ygXx-1QkATPkuPts1Ut-5-jA \
    --set config.kasAddress=wss://kas.gitlab.com


this is the agent config : 

https://gitlab.com/MuhRizkyPerdana/devops-mif_repo/-/tree/main/.gitlab/agents/agentminikube


![alt text](image.png)

![alt text](image-1.png)


# Question Number 5

5. Create ansible file to deploy to target server and repo using your repo devops-mif_repo
from your github personal you previously created
Target_server : 172.1.1.1
Task of ansible : pull source code from your github with repo name repo_devops-mif_repo


this is the ansible playbook i have to playbook, 

clone to dockerhost = https://gitlab.com/MuhRizkyPerdana/devops-mif_repo/-/blob/main/deploy-application.yml
clone and deploy to docker =  https://gitlab.com/MuhRizkyPerdana/devops-mif_repo/-/blob/main/clone-deploy-docker.yml



![alt text](image-3.png)
